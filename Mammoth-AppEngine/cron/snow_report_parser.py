'''
Created on Mar 3, 2013

@author: suneil
'''

import webapp2
import lxml.html
from lxml import etree
from google.appengine.api import urlfetch

from models import SnowReport

class SnowReportParseHandler(webapp2.RequestHandler):
    def get(self):
        url = "http://www.mammothmountain.com/Mountain/Conditions/SnowConditions/"
        result = urlfetch.fetch(url)
        if result.status_code != 200:
            return
        
        doc = lxml.html.document_fromstring(result.content)
        
        for element in doc.xpath('//a[@href="HistoricalSnowfall.aspx"]'):
            element.getparent().remove(element)
            
        elements = doc.xpath('//div[@id="mcSnow"]')
        
        snow_report = ""
        for element in elements:
            snow_report += etree.tostring(element)
            
        latest = SnowReport.query(SnowReport.key_name == 'latest').get()
        if latest is None:
            latest = SnowReport(key_name = 'latest')
        latest.data = snow_report
        latest.put()          

app = webapp2.WSGIApplication([('/cron/parse_snow_report', SnowReportParseHandler)], debug=True)