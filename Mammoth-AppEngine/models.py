'''
Created on Oct 16, 2012

@author: Suneil
'''

from google.appengine.ext import ndb

class SnowReport(ndb.Model):
    key_name = ndb.StringProperty()
    data = ndb.TextProperty()

class Message(ndb.Model):
    msg_date = ndb.StringProperty()
    msg_author = ndb.StringProperty()
    msg_flag = ndb.StringProperty()
    msg_text = ndb.StringProperty()
    img_original = ndb.StringProperty()
    img_small_square = ndb.StringProperty()
    img_large_thumbnail = ndb.StringProperty()
    
    @classmethod
    def property_names(cls):
        names = []

        for name in cls.__dict__:
            attribute = getattr(cls, name)
            if isinstance(attribute, ndb.Property):
                names.append(name)
                
        return names
    
    def to_dict(self):
        obj = {'msg_key':self.key.id(), 'msg_uploaded':1};
        for name in self.property_names():
            attr = getattr(self, name)
            if attr is not None:
                obj[name] = str(attr)

        return obj