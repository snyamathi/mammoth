'''
Created on Mar 3, 2013

@author: suneil
'''

import webapp2
import jinja2
import os

from models import SnowReport

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class GetSnowReportHandler(webapp2.RequestHandler):
    def get(self):
        latest = SnowReport.query(SnowReport.key_name == 'latest').get()
        
        template_values = {
            'snow_report' : latest.data
        }
        
        template = jinja_environment.get_template('snow_report_template.html')
        self.response.out.write(template.render(template_values))