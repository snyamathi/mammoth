'''
Created on Oct 16, 2012

@author: Suneil
'''

import webapp2
import json

from models import Message

def escape(t):
    """HTML-escape the text in `t`."""
    return (t
        .replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        .replace("'", "&#39;").replace('"', "&quot;")
        )

class TextMessageAddHandler(webapp2.RequestHandler):
    def get(self):
        self.error(405)
    def post(self):
        date = self.request.get('msg_date')
        author = self.request.get('msg_author')
        flag = '20'
        text = self.request.get('msg_text')
        message = Message(msg_date = date, msg_author = author, msg_flag = flag, msg_text = text)
        message.put()
        mId = message.key.id()
        self.response.out.write({'msg_key':int(mId)})
        
class PictureMessageAddHandler(webapp2.RequestHandler):
    def get(self):
        self.error(405)
    def post(self):
        date = self.request.get('msg_date')
        author = self.request.get('msg_author')
        flag = '30'
        original = self.request.get('img_original')
        smallSquare = self.request.get('img_small_square')
        largeThumbnail = self.request.get('img_large_thumbnail')
        message = Message(msg_date = date, msg_author = author, msg_flag = flag, img_original = original, img_small_square = smallSquare, img_large_thumbnail = largeThumbnail)
        message.put()
        mId = message.key.id()
        self.response.out.write({'msg_key':int(mId)})
        
class GetMessagesHandler(webapp2.RequestHandler):
    def get(self):
        self.error(405)
    def post(self):
        last = self.request.get('last')
        query = Message.query(Message.msg_date > last).order(Message.msg_date)
        messages = []
        for m in query:
            messages.append(m.to_dict())
        json_response = {"messages" : messages}
        self.response.out.write(json.dumps(json_response))