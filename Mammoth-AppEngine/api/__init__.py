import webapp2
from message import TextMessageAddHandler
from message import PictureMessageAddHandler
from message import GetMessagesHandler
from snow_report import GetSnowReportHandler

app = webapp2.WSGIApplication([('/message/add_text_message', TextMessageAddHandler),
                               ('/message/add_picture_message', PictureMessageAddHandler),
                               ('/message/get_messages', GetMessagesHandler),
                               ('/snow_report/get_snow_report', GetSnowReportHandler)], debug=True)