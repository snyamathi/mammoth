package com.snyamathi.Mammoth.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.snyamathi.Mammoth.R;

public class InteractiveWebcam extends Activity {
	
	private WebView webview;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interactive_webcam);
		webview = (WebView) this.findViewById(R.id.myWebView);
		webview.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url){
				view.loadUrl(url);
				return false;
			}
		});

		WebSettings webViewSettings = webview.getSettings();
		webViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webViewSettings.setJavaScriptEnabled(true);
		webViewSettings.setPluginsEnabled(true);
		webViewSettings.setBuiltInZoomControls(true);
		webViewSettings.setPluginState(PluginState.ON);
		webview.loadUrl("http://ec2-54-245-26-97.us-west-2.compute.amazonaws.com/Village%20Cam/siteproxy.html");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.activity_interactive_webcams, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu_canyon:
	            webview.loadUrl("http://ec2-54-245-26-97.us-west-2.compute.amazonaws.com/Canyon%20Cam/siteproxy.html");
	            return true;
	        case R.id.menu_mccoy:
	            webview.loadUrl("http://ec2-54-245-26-97.us-west-2.compute.amazonaws.com/McCoy/siteproxy.html");
	            return true;
	        case R.id.menu_village:
	        	webview.loadUrl("http://ec2-54-245-26-97.us-west-2.compute.amazonaws.com/Village%20Cam/siteproxy.html");
	            return true;
	        case R.id.menu_main:
	            webview.loadUrl("http://ec2-54-245-26-97.us-west-2.compute.amazonaws.com/Main%20Lodge/siteproxy.html");
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
