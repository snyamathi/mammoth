package com.snyamathi.Mammoth.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;
import com.snyamathi.Mammoth.controller.Controller;
import com.snyamathi.android.common.GifActivity;
import com.snyamathi.android.common.LargeImageActivity;
import com.snyamathi.android.common.WebFrame;

public class Main extends Activity {

	private static final String TAG = Main.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		new Controller(Main.this).getMessages();
		checkVersion();

		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
		int screenWidth = display.getWidth();

		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(new MenuAdapter(Main.this, screenWidth/3));
		gridview.setOnItemClickListener(mListener);
	}

	private void checkVersion() {
		try {			
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Main.this);
			int lastRunVersionCode = prefs.getInt("lastRunVersionCode", 0);

			Editor editor = prefs.edit();

			if (lastRunVersionCode == 0) {
				Log.d(TAG, "New Install");
			} else if (lastRunVersionCode < pInfo.versionCode) {
				Log.d(TAG, "New Update");
			} else {
				Log.d(TAG, "Current");
				return;
			}

			editor.putInt("lastRunVersionCode", pInfo.versionCode);
			editor.commit();
		} catch (NameNotFoundException e) {
			Log.e(TAG, "Error checking version: " + e.getMessage());
		}
	}

	private OnItemClickListener mListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			Intent intent;
			if (id == R.drawable.menu_lift_status) {
				startActivity(new Intent(Main.this, LiftStatusActivity.class));
			} else if (id == R.drawable.menu_snow_report) {
				intent = new Intent(Main.this, WebFrame.class);
				intent.putExtra(Const.URL, Const.URL_SNOW_REPORT);
				startActivity(intent);
			} else if (id == R.drawable.menu_weather) {
				intent = new Intent(Main.this, WebFrame.class);
				intent.putExtra(Const.URL, Const.URL_WEATHER);
				startActivity(intent);
			} else if (id == R.drawable.menu_trail_map) {
				intent = new Intent(Main.this, LargeImageActivity.class);
				intent.putExtra(Const.ASSET, Const.FILENAME_TRAIL_MAP);
				startActivity(intent);
			} else if (id == R.drawable.menu_webcams) {
				startActivity(new Intent(Main.this, Webcams.class));
			} else if (id == R.drawable.menu_road_conditions) {
				intent = new Intent(Main.this, RoadConditions.class);
				startActivity(intent);
			} else if (id == R.drawable.menu_storm_radar) {
				intent = new Intent(Main.this, GifActivity.class);
				intent.putExtra(Const.URL, Const.URL_PAC_SOUTHEST);
				intent.putExtra(Const.FILENAME, Const.FILENAME_PAC_SOUTHWEST);
				startActivity(intent);
			} else if (id == R.drawable.menu_about) {
				startActivity(new Intent(Main.this, About.class));
			} else if (id == R.drawable.menu_your_reports) {
				startActivity(new Intent(Main.this, Forum.class));
			} else if (id == R.drawable.menu_summit_weather) {
				startActivity(new Intent(Main.this, SummitTabs.class));
			} else if (id == R.drawable.menu_bus_map) {
				intent = new Intent(Main.this, LargeImageActivity.class);
				intent.putExtra(Const.ASSET, Const.FILENAME_ROUTE_MAP);
				startActivity(intent);			
			} else if (id == R.drawable.menu_interactive_webcams) {
				startActivity(new Intent(Main.this, InteractiveWebcam.class));
			}
		}
	};
}