package com.snyamathi.Mammoth.view;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;
import com.snyamathi.android.common.LargeImageActivity;

@SuppressWarnings("deprecation")
public class SummitTabs extends TabActivity {
	
	private static final String TEMP_URL = "http://mesowest.utah.edu/cgi-bin/droman/meso_table_chart.cgi?stn=MTH03&unit=0&hours=24&day1=0&month1=&year1=2012&hour1=00&windred=&time=LOCAL&var=TMPF&vnamev=Temperature&stationname=Mammoth%20Gondola%202%20Summit&vlabel=%C2%B0%20F";
	private static final String DEW_URL = "http://mesowest.utah.edu/cgi-bin/droman/meso_table_chart.cgi?stn=MTH03&unit=0&hours=24&day1=0&month1=&year1=2012&hour1=00&windred=&time=LOCAL&var=DWPF&vnamev=Dew%20Point&stationname=Mammoth%20Gondola%202%20Summit&vlabel=%C2%B0%20F";
	private static final String WET_BULB_TEMP = "http://mesowest.utah.edu/cgi-bin/droman/meso_table_chart.cgi?stn=MTH03&unit=0&hours=24&day1=0&month1=&year1=2012&hour1=00&windred=&time=LOCAL&var=TWBF&vnamev=Wet%20Bulb%20Temperature&stationname=Mammoth%20Gondola%202%20Summit&vlabel=%C2%B0%20F";
	private static final String WIND_URL = "http://mesowest.utah.edu/cgi-bin/droman/time_chart.cgi?stn=MTH03&unit=0&month1=&day1=0&year1=2012&hour1=00&timeout=&past=0&hours=12&graph=WND&gsize=1&gauto=1&gmin=&gmax=&linetype=colorline";
	private static final String GUST_URL = "http://mesowest.utah.edu/cgi-bin/droman/meso_table_chart.cgi?stn=MTH03&unit=0&hours=24&day1=0&month1=&year1=2012&hour1=00&windred=&time=LOCAL&var=GUST&vnamev=Wind%20Gust&stationname=Mammoth%20Gondola%202%20Summit&vlabel=%C2%A0mph";
	private static final String HUM_URL = "http://mesowest.utah.edu/cgi-bin/droman/meso_table_chart.cgi?stn=MTH03&unit=0&hours=24&day1=0&month1=&year1=2012&hour1=00&windred=&time=LOCAL&var=RELH&vnamev=Relative%20Humidity&stationname=Mammoth%20Gondola%202%20Summit&vlabel=%";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.summit_tab);

	    TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  			 // Resusable TabSpec for each tab
	    Intent intent;  				 // Reusable Intent for each tab

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    // Initialize a TabSpec for each tab and add it to the TabHost
	    
	    intent = new Intent().setClass(this, LargeImageActivity.class);
	    intent.putExtra(Const.URL, TEMP_URL);
	    spec = tabHost.newTabSpec("Temperature").setIndicator("Temp").setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, LargeImageActivity.class);
	    intent.putExtra(Const.URL, DEW_URL);
	    spec = tabHost.newTabSpec("DewPoint").setIndicator("Dew Point").setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, LargeImageActivity.class);
	    intent.putExtra(Const.URL, WET_BULB_TEMP);
	    spec = tabHost.newTabSpec("WetBulbTemp").setIndicator("Wet Bulb Temp").setContent(intent);
	    tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, LargeImageActivity.class);
	    intent.putExtra(Const.URL, HUM_URL);
	    spec = tabHost.newTabSpec("RelHumidity").setIndicator("Rel. Humidity").setContent(intent);
	    tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, LargeImageActivity.class);
	    intent.putExtra(Const.URL, WIND_URL);
	    spec = tabHost.newTabSpec("WindSpeed").setIndicator("Wind Speed").setContent(intent);
	    tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, LargeImageActivity.class);
	    intent.putExtra(Const.URL, GUST_URL);
	    spec = tabHost.newTabSpec("WindGust").setIndicator("Wind Gust").setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(0);
	}

}
