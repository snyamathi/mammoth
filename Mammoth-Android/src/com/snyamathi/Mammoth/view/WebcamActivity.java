package com.snyamathi.Mammoth.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.fedorvlasov.lazylist.Utils;
import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;
import com.snyamathi.android.common.LargeImageActivity;

public class WebcamActivity extends Activity {

	private static final String[] mccoyUrls = new String[] {
		"http://www.mammothmountain.com/media/images/static_cam/mccoy1.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/mccoy2.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/mccoy3.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/mccoy4.jpg",
	};
	private static final String[] villageUrls = new String[] {
		"http://www.mammothmountain.com/media/images/static_cam/village1.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/village2.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/village3.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/village4.jpg",
	};
	private static final String[] mainUrls = new String[] {
		"http://www.mammothmountain.com/media/images/static_cam/main1.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/main2.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/main3.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/main4.jpg",
	};
	private static final String[] canyonUrls = new String[] {
		"http://www.mammothmountain.com/media/images/static_cam/canyon1.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/canyon2.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/canyon3.jpg",
		"http://www.mammothmountain.com/media/images/static_cam/canyon4.jpg",
	};
	private String[] mUrls;
	private ProgressDialog dialog;
	private Bitmap[] drawables = new Bitmap[4];

	final Handler mHandler = new Handler();
	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			ImageView[] imageViews = new ImageView[] {
					(ImageView) findViewById(R.id.img0), 
					(ImageView) findViewById(R.id.img1), 
					(ImageView) findViewById(R.id.img2),
					(ImageView) findViewById(R.id.img3)
			};

			for (int i = 0; i < 4; i++) {
				final int j = i;
				if (drawables[i] == null) {
					imageViews[i].setImageResource(R.drawable.network_error);
				} else {
					imageViews[i].setImageBitmap(drawables[i]);
					imageViews[i].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = new Intent(WebcamActivity.this, LargeImageActivity.class);
							intent.putExtra(Const.URL, mUrls[j]);
							startActivity(intent);
						}
					});
				}
			}
			dialog.dismiss();
		}	
	};

	protected void onDestroy() {
		super.onDestroy();
		if (dialog.isShowing()) {
			dialog.cancel();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webcam);

		Intent intent = getIntent();
		switch (intent.getExtras().getInt(Const.WEBCAM_LOCATION)) {
		case Const.CANYON:
			mUrls = canyonUrls;
			break;
		case Const.MAIN:
			mUrls = mainUrls;
			break;
		case Const.VILLAGE:
			mUrls = villageUrls;
			break;
		case Const.MCCOY:
			mUrls = mccoyUrls;
			break;
		}

		dialog = ProgressDialog.show(WebcamActivity.this, "", "Loading. Please wait...", true);
		dialog.setCancelable(true);

		Thread t = new Thread() {
			public void run() {		    
				for (int i = 0; i < 4; i++) {
					drawables[i] = Utils.downloadBitmap(mUrls[i]);
				}

				mHandler.post(mUpdateResults);
			}
		};
		dialog.show();
		t.start();
	}

}
