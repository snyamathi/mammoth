package com.snyamathi.Mammoth.view;

import com.snyamathi.Mammoth.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TitleRow extends RelativeLayout {
	
	private View mView;

	public TitleRow(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TitleRow);
        
		boolean largeSize = a.getBoolean(R.styleable.Row_largeRow, false);
		if (largeSize) {
			mView = layoutInflater.inflate(R.layout.title_row_large, this);
		} else {
			mView = layoutInflater.inflate(R.layout.title_row, this);
		}
        
        CharSequence text = a.getString(R.styleable.TitleRow_text);
        if (text != null) {
        	TextView textView = (TextView) mView.findViewById(R.id.txtName);
        	textView.setText(text);
        }
        a.recycle();
	}
}
