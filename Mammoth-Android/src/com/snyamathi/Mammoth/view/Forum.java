package com.snyamathi.Mammoth.view;

import static com.snyamathi.Mammoth.Const.COLUMN_AUTHOR;
import static com.snyamathi.Mammoth.Const.COLUMN_DATE;
import static com.snyamathi.Mammoth.Const.COLUMN_FLAG;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_ORIGINAL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_SMALL_SQUARE;
import static com.snyamathi.Mammoth.Const.COLUMN_TEXT;

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fedorvlasov.lazylist.ImageLoader;
import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;
import com.snyamathi.Mammoth.controller.Controller;
import com.snyamathi.Mammoth.model.MammothDb;
import com.snyamathi.android.common.EllipsizingTextView;
import com.snyamathi.android.common.LargeImageActivity;

public class Forum extends Activity {

	private static final String TAG = Forum.class.getSimpleName();
	private static final int REQUEST_PICK_IMAGE = 0;
	private ImageLoader imageLoader;
	private ProgressDialog dialog; 
	private ListView listview;
	private EditText editText;

	private Handler imageUploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case Const.UPLOAD_IMAGE_FAILURE:
				dialog.setMessage("Image upload failed.  Will try again later.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				dialog.dismiss();
				break;
			case Const.UPLOAD_IMAGE_FETCHING:
				dialog.setMessage("Fetching image from memory...");
				break;
			case Const.UPLOAD_IMAGE_COMPRESSING:
				dialog.setMessage("Compressing image...");
				break;
			case Const.UPLOAD_IMAGE_ENCODING:
				dialog.setMessage("Encoding for upload...");
				break;
			case Const.UPLOAD_IMAGE_SENDING_IMGUR:
				dialog.setMessage("Uploading image...");
				break;
			case Const.UPLOAD_IMAGE_IMGUR_RESPONSE:
				dialog.setMessage("Parsing response...");
				break;
			case Const.UPLOAD_IMAGE_SENDING_GAE:
				dialog.setMessage("Sending to app server...");
				break;
			case Const.UPLOAD_IMAGE_FINISHING:
				dialog.setMessage("Finishing upload...");
				break;
			case Const.UPLOAD_IMAGE_SUCCESS:
				dialog.setMessage("Image uploaded successfully!");
				loadData();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				dialog.dismiss();
				break;
			default:
				break;
			}
		}
	};
	
	private Handler imageTextHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case Const.UPLOAD_TEXT_FAILURE:
				dialog.setMessage("Message upload failed.  Will try again later.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				dialog.dismiss();
				break;
			case Const.UPLOAD_TEXT_UPLOADING:
				dialog.setMessage("Sending message...");
				break;
			case Const.UPLOAD_TEXT_SUCCESS:
				dialog.setMessage("Message sent succesfully!");
				editText.setText("");
				loadData();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				dialog.dismiss();
				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forum);
		new Controller(Forum.this).getMessages();
		listview = (ListView) findViewById(R.id.listView);
		editText = (EditText) findViewById(R.id.edtMessage);
		imageLoader = new ImageLoader(this);
		loadData();
		
		if (PreferenceManager.getDefaultSharedPreferences(this).getString(Const.NAME, null) == null) {
			startActivity(new Intent(this, NameEntry.class));
		}
	}
	
	private void loadData() {
		MammothDb db = new MammothDb(this);
		db.open();
		Cursor cursor = db.getMessages();
		startManagingCursor(cursor);
		listview.setAdapter(new MessageCursorAdapter(this, cursor));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_forum, menu);
		return true;
	}

	public void send(View view) {
		String message = editText.getText().toString().trim();
		if (message == null || message.length() == 0) {
			Toast.makeText(this, "Please enter a message", Toast.LENGTH_SHORT).show();
			return;
		}
		
		String name = PreferenceManager.getDefaultSharedPreferences(this).getString(Const.NAME, "Anonymous");
		
		dialog = new ProgressDialog(this);
		dialog.setMessage("Preparing message upload...");
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.show();
		
		new Controller(this).addTextMessage(name, message, imageTextHandler);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
//		case R.id.menu_camera:
//			new Controller(this).getMessages();
//			return true;
		case R.id.menu_gallery:
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
			return true;
		case R.id.menu_name:
			startActivity(new Intent(this, NameEntry.class));
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_PICK_IMAGE:
			if (resultCode == RESULT_OK) {
				Log.d(TAG, "RESULT_OK");
				Uri uri = data.getData();

				if (uri != null) {
					//User had pick an image.
					Cursor cursor = getContentResolver().query(uri, new String[] {MediaStore.Images.ImageColumns.DATA}, null, null, null);
					cursor.moveToFirst();

					//Link to the image
					final String imageFilePath = cursor.getString(0);
					cursor.close();
					
					String name = PreferenceManager.getDefaultSharedPreferences(this).getString(Const.NAME, "Anonymous");

					dialog = new ProgressDialog(this);
					dialog.setMessage("Preparing image upload...");
					dialog.setIndeterminate(true);
					dialog.setCancelable(true);
					dialog.show();
					Controller controller = new Controller(Forum.this);
					controller.addPictureMessage(name, imageFilePath, imageUploadHandler);
				}
			} else {
				Log.d(TAG, "RESULT_CANCELED");
			}
		}
	}

	private class MessageCursorAdapter extends CursorAdapter {

		private static final int ROW_TEXT = 0;
		private static final int ROW_PICTURE = 1;
		private Cursor mCursor;

		public MessageCursorAdapter(Context context, Cursor cursor) {
			super(context, cursor);
			mCursor = cursor;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView author = (TextView) view.findViewById(R.id.lblAuthor);
			author.setText(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR)));

			TextView timestamp = (TextView) view.findViewById(R.id.lblDate);
			Date date = new Date(cursor.getLong(cursor.getColumnIndex(COLUMN_DATE)));
			DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(Forum.this);
			timestamp.setText(dateFormat.format(date));

			if (view.getTag().equals(200)) {
				EllipsizingTextView text = (EllipsizingTextView)view.findViewById(R.id.lblMessage);
				text.setText(cursor.getString(cursor.getColumnIndex(COLUMN_TEXT)));
			} else if (view.getTag().equals(300)) {
				ImageView imageView = (ImageView) view.findViewById(R.id.imgThumbnail);
				imageLoader.DisplayImage(cursor.getString(cursor.getColumnIndex(COLUMN_IMG_SMALL_SQUARE)), imageView);

				Button viewButton = (Button) view.findViewById(R.id.btnView);
				final String url = cursor.getString(cursor.getColumnIndex(COLUMN_IMG_ORIGINAL));
				viewButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Forum.this, LargeImageActivity.class);
						intent.putExtra(Const.URL, url);
						startActivity(intent);
					}
				});
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(context);
			View v = null;
			if (cursor.getInt(cursor.getColumnIndex(COLUMN_FLAG)) == 200) {
				v = inflater.inflate(R.layout.row_text, parent, false);
				v.setTag(200);
			} else if (cursor.getInt(cursor.getColumnIndex(COLUMN_FLAG)) == 300) {
				v = inflater.inflate(R.layout.row_image, parent, false);
				v.setTag(300);
			}
			return v;
		}

		@Override
		public int getItemViewType(int position) {
			mCursor.moveToPosition(position);
			int flag = mCursor.getInt(mCursor.getColumnIndex(COLUMN_FLAG));
			switch (flag) {
			case 200:
				return ROW_TEXT;
			case 300:
				return ROW_PICTURE;
			}
			return -1;
		}

		@Override
		public int getViewTypeCount() {
			return 2;
		}
	}
}