package com.snyamathi.Mammoth.view;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.WebView;

public class RoadConditions extends Activity {

	private static final String TAG = RoadConditions.class.getSimpleName();
	private static final String URL = "http://www.mammothmountain.com/Mountain/Conditions/Roads/";
	private WebView webview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		webview = new WebView(this);
		webview.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		setContentView(webview);
		
		new DownloadFilesTask().execute();
	}

	private class DownloadFilesTask extends AsyncTask<URL, Integer, String> {

		@Override
		protected void onPreExecute () {
			setProgressBarIndeterminateVisibility(true);
		}

		@Override
		protected String doInBackground(URL... urls) {
			try {
				Document document = Jsoup.parse(new URL(URL), 60000);
				Element element = document.getElementById("mcRoads");
				return element.toString();
			} catch (MalformedURLException e) {
				if (e != null) Log.e(TAG, e.getMessage());
			} catch (IOException e) {
				if (e != null) Log.e(TAG, e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			setProgressBarIndeterminateVisibility(false);
			Log.d(TAG, "Loading HTML");
			webview.loadData(result, "text/html", "utf-8");
		}
	}
}
