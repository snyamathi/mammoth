package com.snyamathi.Mammoth.view;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;
import com.snyamathi.android.common.LargeImageActivity;

public class About extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
    
    public void theAuthor(View v) {
        Random rand = new Random();
        Intent intent = new Intent(this, LargeImageActivity.class);
        intent.putExtra(Const.ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if (rand.nextInt(2) == 0) {
        	intent.putExtra(Const.ASSET, Const.ASSET_ME_ONE);
        } else {
        	intent.putExtra(Const.ASSET, Const.ASSET_ME_TWO);
        }
    	startActivity(intent);
    }
    
    public void launchPaypal(View v) {
    	Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZJA72HFW2JP92"));
    	startActivity(i);
    }
    
}
