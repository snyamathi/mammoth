package com.snyamathi.Mammoth.view;

import com.snyamathi.Mammoth.R;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Row extends RelativeLayout implements OnClickListener {

	private static final String TAG = "Row";

	private View mView;
	private String mLiftNumber;
	private Context mContext;
	private CharSequence[] mArray;

	public Row(Context context, AttributeSet attrs) {
		super(context, attrs);

		mContext = context;
		setClickable(true);
		setOnClickListener(this);
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Row);
		
		boolean largeSize = a.getBoolean(R.styleable.Row_largeRow, false);
		if (largeSize) {
			mView = layoutInflater.inflate(R.layout.row_large, this);
		} else {
			mView = layoutInflater.inflate(R.layout.row, this);
		}
		
		mLiftNumber = a.getString(R.styleable.Row_liftNumber);
		mArray = a.getTextArray(R.styleable.Row_array);

		CharSequence chair = a.getString(R.styleable.Row_chair);
		if (chair != null) {
			TextView textView = (TextView) mView.findViewById(R.id.txtName);
			textView.setText(chair);
		}

		Drawable resouceDrawable = a.getDrawable(R.styleable.Row_src);
		if (resouceDrawable != null) {
			setStatusIcon(resouceDrawable);
		}
		a.recycle();
	}

	public void setStatusIcon(Drawable resouceDrawable) {
		ImageView imageView = (ImageView) mView.findViewById(R.id.imgIcon);
		imageView.setImageDrawable(resouceDrawable);
	}

	public void setTimeUpdated(String time) {
		TextView textView = (TextView) mView.findViewById(R.id.txtUpdated);
		textView.setText(time);
	}

	public String getLiftNumber() {
		return mLiftNumber;
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "onClick");
		if (mArray != null) {
			Intent intent = new Intent(mContext, ChairInfo.class);
			intent.putExtra(ChairInfo.ARRAY_ID, mArray);
			mContext.startActivity(intent);
		}
	}
}
