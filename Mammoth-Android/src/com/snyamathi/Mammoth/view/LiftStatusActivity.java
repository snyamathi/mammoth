package com.snyamathi.Mammoth.view;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;
import com.snyamathi.Mammoth.model.IconMap;
import com.snyamathi.Mammoth.model.LiftStatus;
import com.snyamathi.internal.StatusHandler;

public class LiftStatusActivity extends Activity {
	
	private static final String TAG = LiftStatusActivity.class.getSimpleName();
	private List<Row> rows = new ArrayList<Row>();
	private IconMap iconMap;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.activity_lift_status, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu_view:
				Log.d(TAG, "Changing view type");
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				SharedPreferences.Editor editor = prefs.edit();
				if (showLargeFormat()) {
					editor.putBoolean(Const.PREF_SHOW_LARGE, false);
				} else {
					editor.putBoolean(Const.PREF_SHOW_LARGE, true);
				}
				editor.commit();
				
				Intent intent = new Intent(LiftStatusActivity.this, LiftStatusActivity.class);
				startActivity(intent);
				finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		if (showLargeFormat()) {
			setContentView(R.layout.lift_status_large);
			addChildRows((LinearLayout) findViewById(R.id.llLayout));
		} else {
			setContentView(R.layout.lift_status);		
			addChildRows((LinearLayout) findViewById(R.id.llLeft));
			addChildRows((LinearLayout) findViewById(R.id.llRight));
		}

		iconMap = new IconMap(this);
		new UpdateStatusTask().execute();
		checkIfFirstRun();
	}

	private void addChildRows(LinearLayout ll) {
		for (int i = 0; i < ll.getChildCount(); i++) {
			View view = ll.getChildAt(i);
			if (view instanceof Row) {
				rows.add((Row) view);
			}
		}
	}

	private boolean showLargeFormat() {
		return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Const.PREF_SHOW_LARGE, false);
	}

	private void checkIfFirstRun() {
		SharedPreferences prefs = getSharedPreferences("LiftStatus", 0);
		boolean firstRun = prefs.getBoolean("firstRun", true);

		if (firstRun) {
			Log.d(TAG, "First run of " + TAG);
			AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
			alertbox.setMessage(R.string.liftHelp);
			alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {  		 
				// do something when the button is clicked
				public void onClick(DialogInterface arg0, int arg1) {
					SharedPreferences prefs = getSharedPreferences("LiftStatus", 0);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putBoolean("firstRun", false);
					// Commit the edits!
					editor.commit();
				}
			});
			alertbox.show();
		}
	}

	private class UpdateStatusTask extends AsyncTask<URL, Integer, Long> {

		private boolean errorOccurred = false;
		private Map<String, LiftStatus> statusMap;

		@Override
		protected void onPreExecute () {
			setProgressBarIndeterminateVisibility(true);
			statusMap = new HashMap<String, LiftStatus>();
		}

		@Override
		protected Long doInBackground(URL... urls) {
			try {
				Log.d(TAG, "Fetching updated lift status");
				final URL url = new URL(StatusHandler.URL);
				final SAXParserFactory spf = SAXParserFactory.newInstance();
				final SAXParser sp = spf.newSAXParser();	
				final XMLReader xr = sp.getXMLReader();
				final StatusHandler handler = new StatusHandler();

				xr.setContentHandler(handler);
				xr.parse(new InputSource(url.openStream()));

				statusMap = handler.getStatusMap();
			} catch (Exception e) {
				Log.e(TAG, "Update failed: " + e.getMessage());
				errorOccurred = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Long result) {
			if (errorOccurred) {
				Toast.makeText(LiftStatusActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
				return;
			}

			Log.d(TAG, "Update successful");
			for (Row row : rows) {
				LiftStatus status = statusMap.get(row.getLiftNumber());
				if (status == null) {
					continue;
				}
				row.setStatusIcon(iconMap.get(status.getStatusCode()));
				row.setTimeUpdated(status.getUpdated());
			}
			setProgressBarIndeterminateVisibility(false);
		}
	}
}