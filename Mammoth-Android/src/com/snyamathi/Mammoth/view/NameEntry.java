package com.snyamathi.Mammoth.view;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

public class NameEntry extends Activity {

	private EditText edtName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_name);
		
		edtName = (EditText) findViewById(R.id.edtPassword);
		edtName.setText(PreferenceManager.getDefaultSharedPreferences(this).getString(Const.NAME, "Anonymous"));
		setResult(Activity.RESULT_CANCELED);
	}
	
	public void cancel(View view) {
		finish();
	}	

	public void okay(View view) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor = prefs.edit();
		editor.putString(Const.NAME, edtName.getText().toString().trim());
		editor.commit();
		finish();
	}
}