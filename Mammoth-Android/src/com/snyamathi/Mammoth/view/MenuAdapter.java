package com.snyamathi.Mammoth.view;

import com.snyamathi.Mammoth.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MenuAdapter extends BaseAdapter {
	private Context mContext;
	private int mIconSize;
	private static final Integer[] mThumbIds = {
		R.drawable.menu_lift_status, 
		R.drawable.menu_snow_report,
		R.drawable.menu_weather, 
		R.drawable.menu_trail_map,
		R.drawable.menu_webcams, 
		R.drawable.menu_road_conditions, 
		R.drawable.menu_storm_radar, 
		R.drawable.menu_about, 
		R.drawable.menu_your_reports,
		R.drawable.menu_summit_weather,
		R.drawable.menu_bus_map,
		R.drawable.menu_interactive_webcams
	};

	public MenuAdapter(Context context, int size) {
		mContext = context;
		mIconSize = size;
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return mThumbIds[position];
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) {
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(mIconSize, mIconSize));
		} else {
			imageView = (ImageView) convertView;
		}

		imageView.setImageResource(mThumbIds[position]);
		return imageView;
	} 
}