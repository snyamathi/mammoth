package com.snyamathi.Mammoth.view;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;

@SuppressWarnings("deprecation")
public class Webcams extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.webcams);

	    Resources res = getResources(); // Resource object to get Drawables
		TabHost tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, WebcamActivity.class);
	    intent.putExtra(Const.WEBCAM_LOCATION, Const.MAIN);
	    spec = tabHost.newTabSpec("artists").setIndicator("Main", res.getDrawable(R.drawable.ic_tab_main)).setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, WebcamActivity.class);
	    intent.putExtra(Const.WEBCAM_LOCATION, Const.CANYON);
	    spec = tabHost.newTabSpec("albums").setIndicator("Canyon", res.getDrawable(R.drawable.ic_tab_canyon)).setContent(intent);
	    tabHost.addTab(spec);

	    intent = new Intent().setClass(this, WebcamActivity.class);
	    intent.putExtra(Const.WEBCAM_LOCATION, Const.VILLAGE);
	    spec = tabHost.newTabSpec("songs").setIndicator("Village", res.getDrawable(R.drawable.ic_tab_village)).setContent(intent);
	    tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, WebcamActivity.class);
	    intent.putExtra(Const.WEBCAM_LOCATION, Const.MCCOY);
	    spec = tabHost.newTabSpec("albums").setIndicator("McCoy", res.getDrawable(R.drawable.ic_tab_mccoy)).setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(2);
	}
	
}
