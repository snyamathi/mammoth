package com.snyamathi.Mammoth.view;

import com.snyamathi.Mammoth.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ChairInfo extends Activity {
	
	public static final String ARRAY_ID = "ARRAY_ID";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chair_info);
        
        Intent intent = getIntent();
        CharSequence[] array = intent.getCharSequenceArrayExtra(ARRAY_ID);
        
        TextView txtNo = (TextView)findViewById(R.id.txtNo);
        TextView txtType = (TextView)findViewById(R.id.txtType);
        TextView txtDate = (TextView)findViewById(R.id.txtDate);
        TextView txtCapacity = (TextView)findViewById(R.id.txtCapacity);
        TextView txtLength = (TextView)findViewById(R.id.txtLength);
        TextView txtVertical = (TextView)findViewById(R.id.txtVertical);
        TextView txtTime = (TextView)findViewById(R.id.txtTime);
        TextView txtAccess = (TextView)findViewById(R.id.txtAccess);
        TextView txtNearby = (TextView)findViewById(R.id.txtNearby);
        TextView txtComments = (TextView)findViewById(R.id.txtComments);
        TextView txtInfo = (TextView)findViewById(R.id.txtInfo);
        
        ChairInfo.this.setTitle(array[0]);
        txtNo.setText(array[1]);
        txtType.setText(array[2]);
        txtDate.setText(array[3]);
        txtCapacity.setText(array[4]);
        txtLength.setText(array[5]);
        txtVertical.setText(array[6]);
        txtTime.setText(array[7]);
        txtAccess.setText(array[8]);
        txtNearby.setText(array[9]);
        txtComments.setText(array[10]);
        txtInfo.setText(array[11]);
    }
}