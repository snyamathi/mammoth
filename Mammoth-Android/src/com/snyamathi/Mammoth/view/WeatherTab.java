package com.snyamathi.Mammoth.view;

import java.io.InputStream;
import java.net.URL;

import com.snyamathi.Mammoth.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

public class WeatherTab extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.weather_tab);

		Intent intent = getIntent();
		String URL = intent.getStringExtra("URL");
		new LoadImageTask().execute(URL);

	}

	private class LoadImageTask extends AsyncTask<String, Integer, Drawable> {
		private ProgressDialog dialog;
		
		@Override
		protected void onPreExecute () {
			dialog = ProgressDialog.show(WeatherTab.this, "", "Loading. Please wait...", true);
			dialog.setCancelable(true);
			dialog.show();
		}

		@Override
		protected Drawable doInBackground(String... url) {
			try {
				InputStream is = (InputStream) new URL(url[0]).getContent();
				return Drawable.createFromStream(is, "src name");
			} catch (Exception e) {
				return getResources().getDrawable(R.drawable.network_error);
			}
		}

		@Override
		protected void onPostExecute(Drawable drawable) {
			ImageView iv = (ImageView) findViewById(R.id.imageView);
			iv.setImageDrawable(drawable);
			dialog.dismiss();
		}
	}
}