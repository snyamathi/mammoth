package com.snyamathi.Mammoth.controller;

import static com.snyamathi.Mammoth.Const.COLUMN_AUTHOR;
import static com.snyamathi.Mammoth.Const.COLUMN_DATE;
import static com.snyamathi.Mammoth.Const.COLUMN_FLAG;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_LARGE_THUMBNAIL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_ORIGINAL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_SMALL_SQUARE;
import static com.snyamathi.Mammoth.Const.COLUMN_KEY;
import static com.snyamathi.Mammoth.Const.COLUMN_TEXT;
import static com.snyamathi.Mammoth.Const.PREF_LAST_DATE;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.model.MammothDb;
import com.snyamathi.Mammoth.model.PictureMessage;
import com.snyamathi.Mammoth.model.TextMessage;
import com.snyamathi.android.common.ImageUtils;

public class Controller {

	private static final String TAG = Controller.class.getSimpleName();
	private static final String API_KEY = "c695fcd2a41358bd4d401cbf270d09aa";
	private Context mContext;

	public Controller(Context context) {
		mContext = context;
	}

	public void addTextMessage(final String name, final String text, final Handler handler) {
		Log.i(TAG, "addTextMessage()");
		new Thread(new Runnable() {

			@Override
			public void run() {
				handler.obtainMessage(Const.UPLOAD_TEXT_UPLOADING).sendToTarget();
				TextMessage m = new TextMessage();
				m.setAuthor(name);
				m.setFlag(200);
				m.setDate(System.currentTimeMillis());
				m.setText(text);

				// First insert the message
				MammothDb db = new MammothDb(mContext);
				db.open();
				long rowId = db.insertTextMessage(m);
				m.setId(rowId);

				// Then upload it to AppEngine
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
				nameValuePairs.add(new BasicNameValuePair(COLUMN_DATE, Long.toString(m.getDate())));
				nameValuePairs.add(new BasicNameValuePair(COLUMN_AUTHOR, m.getAuthor()));
				nameValuePairs.add(new BasicNameValuePair(COLUMN_FLAG, Integer.toString(m.getFlag())));
				nameValuePairs.add(new BasicNameValuePair(COLUMN_TEXT, m.getText()));
				
				try {
					String result = postData("http://snyamathi-mammoth.appspot.com/message/add_text_message", nameValuePairs);
					
					JSONObject json = new JSONObject(result);
					m.setKey(json.getLong(COLUMN_KEY));
					db.markMessageUploaded(m.getId(), m.getKey());
					handler.obtainMessage(Const.UPLOAD_TEXT_SUCCESS).sendToTarget();
				} catch (JSONException e) {
					Log.e(TAG, "Error uploading message");
					handler.obtainMessage(Const.UPLOAD_TEXT_FAILURE).sendToTarget();
				} catch (IOException e) {
					if (e != null) Log.e(TAG, e.getMessage());
					handler.obtainMessage(Const.UPLOAD_TEXT_FAILURE).sendToTarget();
				}

				db.close();
			}
		}).start();
	}

	public void addPictureMessage(final String name, final String imagePath, final Handler handler) {
		Log.i(TAG, "addPictureMessage()");
		new Thread(new Runnable() {

			@Override
			public void run() {
				handler.obtainMessage(Const.UPLOAD_IMAGE_FETCHING).sendToTarget();
				final BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(imagePath, options);
				options.inSampleSize = ImageUtils.calculateInSampleSize(options, 200, 200);

				handler.obtainMessage(Const.UPLOAD_IMAGE_COMPRESSING).sendToTarget();
				options.inJustDecodeBounds = false;
				Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
				bitmap.recycle();

				handler.obtainMessage(Const.UPLOAD_IMAGE_ENCODING).sendToTarget();
				byte[] byteArrayImage = outStream.toByteArray();
				String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
				nameValuePairs.add(new BasicNameValuePair("key", API_KEY));
				nameValuePairs.add(new BasicNameValuePair("image", encodedImage));
				nameValuePairs.add(new BasicNameValuePair("type", "base64"));

				

				try {
					handler.obtainMessage(Const.UPLOAD_IMAGE_SENDING_IMGUR).sendToTarget();
					String response = postData("http://api.imgur.com/2/upload.json", nameValuePairs);
					
					handler.obtainMessage(Const.UPLOAD_IMAGE_IMGUR_RESPONSE).sendToTarget();
					JSONObject links = new JSONObject(response).getJSONObject("upload").getJSONObject("links");
					String original = links.getString("original");
					String smallSquare = links.getString("small_square");
					String largeThumbnail = links.getString("large_thumbnail");

					PictureMessage m = new PictureMessage();
					m.setAuthor(name);
					m.setDate(System.currentTimeMillis());
					m.setImgOriginal(original);
					m.setFlag(300);
					m.setImgSmallSquare(smallSquare);
					m.setImgLargeThumbnail(largeThumbnail);

					// First insert the message
					MammothDb db = new MammothDb(mContext);
					db.open();
					long rowId = db.insertPictureMessage(m);
					m.setId(rowId);


					// Then upload it to AppEngine
					handler.obtainMessage(Const.UPLOAD_IMAGE_SENDING_GAE).sendToTarget();
					nameValuePairs.clear();
					nameValuePairs.add(new BasicNameValuePair(COLUMN_DATE, Long.toString(m.getDate())));
					nameValuePairs.add(new BasicNameValuePair(COLUMN_AUTHOR, m.getAuthor()));
					nameValuePairs.add(new BasicNameValuePair(COLUMN_FLAG, Integer.toString(m.getFlag())));
					nameValuePairs.add(new BasicNameValuePair(COLUMN_IMG_ORIGINAL, m.getImgOriginal()));
					nameValuePairs.add(new BasicNameValuePair(COLUMN_IMG_SMALL_SQUARE, m.getImgSmallSquare()));
					nameValuePairs.add(new BasicNameValuePair(COLUMN_IMG_LARGE_THUMBNAIL, m.getImgLargeThumbnail()));
					response = postData("http://snyamathi-mammoth.appspot.com/message/add_picture_message", nameValuePairs);

					handler.obtainMessage(Const.UPLOAD_IMAGE_FINISHING).sendToTarget();
					JSONObject json = new JSONObject(response);
					m.setKey(json.getLong(COLUMN_KEY));
					db.markMessageUploaded(m.getId(), m.getKey());
					handler.obtainMessage(Const.UPLOAD_IMAGE_SUCCESS).sendToTarget();
				} catch (JSONException e){
					Log.e(TAG, e.getMessage());
					handler.obtainMessage(Const.UPLOAD_IMAGE_FAILURE).sendToTarget();
				} catch (IOException e) {
					if (e != null) Log.e(TAG, e.getMessage());
					handler.obtainMessage(Const.UPLOAD_IMAGE_FAILURE).sendToTarget();
				}
			}
		}).start();
	}

	public void getMessages() {
		Log.i(TAG, "getMessages()");

		new Thread(new Runnable() {

			@Override
			public void run() {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
				long lastDate = prefs.getLong(PREF_LAST_DATE, 0);
				Editor editor = prefs.edit();

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				nameValuePairs.add(new BasicNameValuePair("last", Long.toString(lastDate)));
				String response = null;
				try {
					response = postData("http://snyamathi-mammoth.appspot.com/message/get_messages", nameValuePairs);
				} catch (IOException e1) {
					if (e1 != null) Log.e(TAG, e1.getMessage());
				}

				if (response == null) {
					Log.e(TAG, "Null response from GAE");
					return;
				}

				try {

					JSONObject object = new JSONObject(response);
					JSONArray array = object.getJSONArray("messages");

					Gson gson = new Gson();
					MammothDb db = new MammothDb(mContext);
					db.open();
					for (int i = 0; i < array.length(); i++) {
						JSONObject item = array.getJSONObject(i);
						if (item.getInt(COLUMN_FLAG) == 200) {
							Log.i(TAG, "Adding text message");
							TextMessage m = gson.fromJson(item.toString(), TextMessage.class);
							db.insertTextMessage(m);
							editor.putLong(PREF_LAST_DATE, m.getDate());
						} else if (item.getInt(COLUMN_FLAG) == 300) {
							Log.i(TAG, "Adding picture message message");
							PictureMessage m = gson.fromJson(item.toString(), PictureMessage.class);
							db.insertPictureMessage(m);
							editor.putLong(PREF_LAST_DATE, m.getDate());
						}
					}
					db.close();
				} catch (JSONException e){
					Log.e(TAG, e.getMessage());
				} finally {
					editor.commit();
				}
			}
		}).start();
	}

	public String postData(String url, List<NameValuePair> nameValuePairs) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse response = httpclient.execute(httppost);
		String responseString = EntityUtils.toString(response.getEntity()); 
		return responseString;
	}

	public static int getNumberLiftsOpen() {
		return 5;
	}
}
