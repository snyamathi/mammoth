package com.snyamathi.Mammoth.model;

import static com.snyamathi.Mammoth.Const.*;

import com.google.gson.annotations.SerializedName;

public class TextMessage extends Message {

	@SerializedName(COLUMN_TEXT)
	private String text;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

}
