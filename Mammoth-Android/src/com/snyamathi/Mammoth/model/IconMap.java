package com.snyamathi.Mammoth.model;

import java.util.HashMap;

import com.snyamathi.Mammoth.R;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class IconMap extends HashMap<String, Drawable> {
	
	private static final long serialVersionUID = 4559018704770157564L;
	
	public IconMap(Context context) {
		put("O", context.getResources().getDrawable(R.drawable.status_open));
		put("C", context.getResources().getDrawable(R.drawable.status_closed));
		put("W30", context.getResources().getDrawable(R.drawable.status_expected));
		put("HE", context.getResources().getDrawable(R.drawable.status_weather_hold));
		put("?", context.getResources().getDrawable(R.drawable.stub));
	}
	
	@Override
	public Drawable get(Object key) {
		Drawable drawable = super.get(key);
		if (drawable == null) {
			return get("?");
		} else {
			return drawable;
		}
	}
}
