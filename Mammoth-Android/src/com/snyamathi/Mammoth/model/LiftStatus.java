package com.snyamathi.Mammoth.model;

public class LiftStatus {

	public static final String OPEN = "O";
	public static final String CLOSED = "C";
	public static final String SOON = "W30";
	public static final String HOLD_EXPECTED = "HE";

	private final String id;
	private final String statusCode;
	private final String updated;

	public LiftStatus (String id, String statusCode, String updated) {
		this.id = id;
		this.statusCode = statusCode;
		this.updated = updated;
	}

	public String getId() {
		return id;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public String getUpdated() {
		return updated;
	}
}