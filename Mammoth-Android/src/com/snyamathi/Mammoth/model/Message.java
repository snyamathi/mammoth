package com.snyamathi.Mammoth.model;

import static com.snyamathi.Mammoth.Const.COLUMN_AUTHOR;
import static com.snyamathi.Mammoth.Const.COLUMN_DATE;
import static com.snyamathi.Mammoth.Const.COLUMN_FLAG;
import static com.snyamathi.Mammoth.Const.COLUMN_ID;
import static com.snyamathi.Mammoth.Const.COLUMN_KEY;
import static com.snyamathi.Mammoth.Const.COLUMN_UPLOADED;

import com.google.gson.annotations.SerializedName;

public class Message {
	
	@SerializedName(COLUMN_ID)
	private long id;
	
	@SerializedName(COLUMN_KEY)
	private long key;
	
	@SerializedName(COLUMN_DATE)
	private long date;
	
	@SerializedName(COLUMN_AUTHOR)
	private String author;
	
	@SerializedName(COLUMN_FLAG)
	private int flag;
	
	@SerializedName(COLUMN_UPLOADED)
	private int uploaded;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getKey() {
		return key;
	}
	public void setKey(long key) {
		this.key = key;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public int getUploaded() {
		return uploaded;
	}
	public void setUploaded(int uploaded) {
		this.uploaded = uploaded;
	}
}
