package com.snyamathi.Mammoth.model;

import static com.snyamathi.Mammoth.Const.COLUMN_AUTHOR;
import static com.snyamathi.Mammoth.Const.COLUMN_DATE;
import static com.snyamathi.Mammoth.Const.COLUMN_FLAG;
import static com.snyamathi.Mammoth.Const.COLUMN_ID;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_LARGE_THUMBNAIL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_ORIGINAL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_SMALL_SQUARE;
import static com.snyamathi.Mammoth.Const.COLUMN_KEY;
import static com.snyamathi.Mammoth.Const.COLUMN_TEXT;
import static com.snyamathi.Mammoth.Const.COLUMN_UPLOADED;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MammothDb {

	private static final String TAG = MammothDb.class.getSimpleName();
	private static final String TABLE_MESSAGE = "message";
	private static final String TABLE_PICTURE_MESSAGE = "picture_message";
	private static final String TABLE_TEXT_MESSAGE = "text_message";

	private MammothDbOpenHelper dbHelper;
	private SQLiteDatabase database;

	public MammothDb(Context context) {
		dbHelper = new MammothDbOpenHelper(context);
	}

	public void open() throws SQLException {
		Log.v(TAG, "open()");
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		Log.v(TAG, "close()");
		dbHelper.close();
	}
	
	public boolean messageExists(Message m) {
		Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM "+TABLE_MESSAGE+" WHERE "+COLUMN_KEY+"="+m.getKey(), null);
		if (cursor.moveToFirst()) {
			return cursor.getInt(0) > 0	;		
		}
		return false;
	}

	public long insertTextMessage(TextMessage m) {
		if (messageExists(m)) {
			return -1;
		}
		
		ContentValues values = new ContentValues();
		values.put(COLUMN_KEY, m.getKey());
		values.put(COLUMN_DATE, m.getDate());
		values.put(COLUMN_AUTHOR, m.getAuthor());
		values.put(COLUMN_FLAG, m.getFlag());
		values.put(COLUMN_UPLOADED, m.getUploaded());
		long rowId = database.insert(TABLE_MESSAGE, null, values);

		values = new ContentValues();
		values.put(COLUMN_ID, rowId);
		values.put(COLUMN_TEXT, m.getText());
		database.insert(TABLE_TEXT_MESSAGE, null, values);

		return rowId;
	}

	public long insertPictureMessage(PictureMessage m) {
		if (messageExists(m)) {
			return -1;
		}
		
		ContentValues values = new ContentValues();
		values.put(COLUMN_KEY, m.getKey());
		values.put(COLUMN_DATE, m.getDate());
		values.put(COLUMN_AUTHOR, m.getAuthor());
		values.put(COLUMN_FLAG, m.getFlag());
		values.put(COLUMN_UPLOADED, m.getUploaded());
		long rowId = database.insert(TABLE_MESSAGE, null, values);

		values = new ContentValues();
		values.put(COLUMN_ID, rowId);
		values.put(COLUMN_IMG_ORIGINAL, m.getImgOriginal());
		values.put(COLUMN_IMG_SMALL_SQUARE, m.getImgSmallSquare());
		values.put(COLUMN_IMG_LARGE_THUMBNAIL, m.getImgLargeThumbnail());
		database.insert(TABLE_PICTURE_MESSAGE, null, values);

		return rowId;
	}

	public long markMessageUploaded(long id, long key) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_KEY, key);
		values.put(COLUMN_UPLOADED, 1);
		return database.update(TABLE_MESSAGE, values, COLUMN_ID + "=" + id, null);
	}

	private class MammothDbOpenHelper extends SQLiteOpenHelper {

		private static final String DATABASE_NAME = "MammothDb";
		private static final int DATABASE_VERSION = 1;

		private static final String TABLE_MESSAGE_CREATE = 
				"CREATE TABLE "+TABLE_MESSAGE+
				"("+
				COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
				COLUMN_KEY+" INTEGER, "+
				COLUMN_AUTHOR+" TEXT, "+
				COLUMN_DATE+" INTEGER, "+
				COLUMN_FLAG+" INTEGER, "+
				COLUMN_UPLOADED+" INTEGER"+
				");";

		private static final String TABLE_TEXT_MESSAGE_CREATE = 
				"CREATE TABLE "+TABLE_TEXT_MESSAGE+
				"("+
				COLUMN_ID+" INTEGER, "+
				COLUMN_TEXT+" TEXT, "+
				"FOREIGN KEY("+COLUMN_ID+") REFERENCES "+TABLE_MESSAGE+"("+COLUMN_ID+") ON DELETE CASCADE"+
				");";

		private static final String TABLE_PICTURE_MESSAGE_CREATE = 
				"CREATE TABLE "+TABLE_PICTURE_MESSAGE+
				"("+
				COLUMN_ID+" INTEGER, "+
				COLUMN_IMG_ORIGINAL+" TEXT, "+
				COLUMN_IMG_SMALL_SQUARE+" TEXT, "+
				COLUMN_IMG_LARGE_THUMBNAIL+" TEXT, "+
				"FOREIGN KEY("+COLUMN_ID+") REFERENCES "+TABLE_MESSAGE+"("+COLUMN_ID+") ON DELETE CASCADE"+
				");";

		public MammothDbOpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onOpen(SQLiteDatabase db) {
			super.onOpen(db);
			if (!db.isReadOnly()) {
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.v(TAG, "onCreate()");

			Log.d(TAG, "execSQL(TABLE_MESSAGE_CREATE)");
			db.execSQL(TABLE_MESSAGE_CREATE);

			Log.d(TAG, "execSQL(TABLE_TEXT_MESSAGE_CREATE)");
			db.execSQL(TABLE_TEXT_MESSAGE_CREATE);

			Log.d(TAG, "execSQL(TABLE_PICTURE_MESSAGE_CREATE)");
			db.execSQL(TABLE_PICTURE_MESSAGE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.v(TAG, "onUpgrade()");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGE);
			onCreate(db);
		}
	}

	public Cursor getMessages() {
		final String query = 
				"SELECT message._id, "+
						"message.msg_key, "+
						"message.msg_author, "+ 
						"message.msg_date, " +
						"message.msg_flag, " +
						"message.msg_uploaded, " +
						"text_message.msg_text, " +
						"picture_message.img_original, " +
						"picture_message.img_small_square, " +
						"picture_message.img_large_thumbnail "+ 
						"FROM message "+
						"LEFT JOIN text_message ON message._id = text_message._id "+
						"LEFT JOIN picture_message ON message._id = picture_message._id "+
						"ORDER BY msg_date DESC;"; 
		return database.rawQuery(query, null);
	}
}