package com.snyamathi.Mammoth.model;

import static com.snyamathi.Mammoth.Const.COLUMN_IMG_LARGE_THUMBNAIL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_ORIGINAL;
import static com.snyamathi.Mammoth.Const.COLUMN_IMG_SMALL_SQUARE;

import com.google.gson.annotations.SerializedName;

public class PictureMessage extends Message {
	
	@SerializedName(COLUMN_IMG_ORIGINAL)
	private String imgOriginal;
	
	@SerializedName(COLUMN_IMG_SMALL_SQUARE)
	private String imgSmallSquare;
	
	@SerializedName(COLUMN_IMG_LARGE_THUMBNAIL)
	private String imgLargeThumbnail;
	
	public String getImgOriginal() {
		return imgOriginal;
	}
	public void setImgOriginal(String imgOriginal) {
		this.imgOriginal = imgOriginal;
	}
	public String getImgSmallSquare() {
		return imgSmallSquare;
	}
	public void setImgSmallSquare(String imgSmallSquare) {
		this.imgSmallSquare = imgSmallSquare;
	}
	public String getImgLargeThumbnail() {
		return imgLargeThumbnail;
	}
	public void setImgLargeThumbnail(String imgLargeThumbnail) {
		this.imgLargeThumbnail = imgLargeThumbnail;
	}

}
