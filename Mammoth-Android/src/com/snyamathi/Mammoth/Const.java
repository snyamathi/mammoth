package com.snyamathi.Mammoth;

public class Const {
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_KEY = "msg_key";
	public static final String COLUMN_DATE = "msg_date";
	public static final String COLUMN_AUTHOR = "msg_author";
	public static final String COLUMN_FLAG = "msg_flag";
	public static final String COLUMN_TEXT = "msg_text";
	public static final String COLUMN_PATH = "msg_path";
	public static final String COLUMN_EXTRAS = "msg_extras";
	public static final String COLUMN_UPLOADED = "msg_uploaded";
	public static final String COLUMN_IMG_ORIGINAL = "img_original";
	public static final String COLUMN_IMG_SMALL_SQUARE = "img_small_square";
	public static final String COLUMN_IMG_LARGE_THUMBNAIL = "img_large_thumbnail";
	
	public static final String PREF_AUTHOR = "pref_author";
	public static final String PREF_LAST_DATE = "pref_last_date";
	public static final String PREF_SHOW_LARGE = "pref_show_large";
	
	public static final int UPDATE_COMPLETE = 0; 

	public static final String URL = "url";
	public static final String ORIENTATION = "orientation";
	public static final String ASSET = "asset";
	public static final String FILENAME = "filename";
	
	public static final String URL_SNOW_REPORT = "http://snyamathi-mammoth.appspot.com/snow_report/get_snow_report";
	public static final String URL_GROOMING = "http://www.mammothmountain.com/Mountain/Conditions/TrailStatus/?SortOrder=Location";
	public static final String URL_ROADS = "http://www.mammothmountain.com/Mountain/Conditions/Roads/";
	public static final String URL_WEATHER = "http://forecast.weather.gov/MapClick.php?lat=37.66&lon=-118.97&site=rev&smap=1&unit=0&lg=en&FcstType=text&TextType=2";
	
	public static final String URL_PAC_SOUTHEST = "http://radar.weather.gov/ridge/Conus/Loop/pacsouthwest_loop.gif";
	public static final String FILENAME_PAC_SOUTHWEST = "pacsouthwest_loop.gif";
	
	public static final String URL_ROUTE_MAP = "http://dl.dropbox.com/u/9649335/Mammoth/route_map.png";
	public static final String FILENAME_ROUTE_MAP = "route_map.png";
	
	public static final String URL_TRAIL_MAP = "http://dl.dropbox.com/u/9649335/Mammoth/trail_map.jpg";
	public static final String FILENAME_TRAIL_MAP = "trail_map.jpg";
	
	public static final String ASSET_ME_ONE = "me_one.jpg";
	public static final String ASSET_ME_TWO = "me_two.jpg";
	
	public static final int UPLOAD_IMAGE_FAILURE = -1;
	public static final int UPLOAD_IMAGE_FETCHING = 1;
	public static final int UPLOAD_IMAGE_COMPRESSING = 2;
	public static final int UPLOAD_IMAGE_ENCODING = 3;
	public static final int UPLOAD_IMAGE_SENDING_IMGUR = 4;
	public static final int UPLOAD_IMAGE_IMGUR_RESPONSE = 5;
	public static final int UPLOAD_IMAGE_SENDING_GAE = 6;
	public static final int UPLOAD_IMAGE_FINISHING = 7;
	public static final int UPLOAD_IMAGE_SUCCESS = 8;
	
	public static final int UPLOAD_TEXT_FAILURE = -1;
	public static final int UPLOAD_TEXT_UPLOADING = 1;
	public static final int UPLOAD_TEXT_SUCCESS = 2;
	
	// Forum Constants
	public static final String NAME = "name";
	public static final String WEBCAM_LOCATION = "webcam_locations";
	public static final int CANYON = 1;
	public static final int MAIN = 2;
	public static final int VILLAGE = 3;
	public static final int MCCOY = 4;
}