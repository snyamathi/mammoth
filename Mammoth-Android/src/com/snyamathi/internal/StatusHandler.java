package com.snyamathi.internal;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.text.format.Time;
import android.util.Log;

import com.snyamathi.Mammoth.model.LiftStatus;

public class StatusHandler extends DefaultHandler {

	public static final String URL = "http://www.mammothmountain.com/e/Mmsa.Data.MountainOps.Orm/Xml/UI/LiftStatus.ascx";
	
	private static final String LIFT = "Lift";
	private static final String ID = "ID";
	private static final String STATUS_CODE = "StatusCode";
	private static final String UPDATED = "Updated";
	
	private static final String TAG = "StatusHandler";

	private Map<String, LiftStatus> statusMap;

	public StatusHandler() {
		statusMap = new HashMap<String, LiftStatus>();
		statusMap.put("Open", new LiftStatus("Open", LiftStatus.OPEN, ""));
		statusMap.put("Closed", new LiftStatus("Closed", LiftStatus.CLOSED, ""));
		statusMap.put("30 Minutes or less!", new LiftStatus("30 Minutes or less!", LiftStatus.SOON, ""));
		statusMap.put("Hold / Expected", new LiftStatus("Hold / Expected", LiftStatus.HOLD_EXPECTED, ""));
	}

	public Map<String, LiftStatus> getStatusMap() {
		return statusMap;
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (localName.equals(LIFT)) {
			String id = atts.getValue(ID);
			String statusCode = atts.getValue(STATUS_CODE);
			String updated = "";
			try {
				String temp = atts.getValue(UPDATED);
				temp = temp.subSequence(0, temp.length()-3)  +":00.000-00:00";
				Time time = new Time();
				time.parse3339(temp);

				Time yesterday = new Time();
				yesterday.set(System.currentTimeMillis() - 86400000);

				if (time.after(yesterday)) {
					updated = time.format("%H:%M");
				} else {
					updated = "N/A";
				}
			} catch (Exception e) {
				Log.e(TAG, "Error parsing time: " + e.getMessage());
			}
			LiftStatus status = new LiftStatus(id, statusCode, updated);
			statusMap.put(id, status);
		}
	}
}