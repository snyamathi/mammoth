package com.snyamathi.android.common;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;

public class GifActivity extends Activity {

	private static final String TAG = GifActivity.class.getSimpleName();

	private String mUrl;
	private String mFilename;
	private String mFilepath;
	private LargeImageView mLargeImageView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_large_image);

		Intent intent = getIntent();
		mUrl = intent.getStringExtra(Const.URL);
		mFilename = intent.getStringExtra(Const.FILENAME);
		mFilepath = getFileStreamPath(mFilename).getAbsolutePath();
		mLargeImageView = (LargeImageView) findViewById(R.id.LargeImageView);

		if (new File(mFilepath).canRead()) {
			mLargeImageView.loadImage("file://" + mFilepath);
		}

		new DownloadGifTask().execute();
	}
	
	@Override
	protected void onPause() {
	    super.onPause();
	    finish();
	}
	
	private class DownloadGifTask extends AsyncTask<String, Integer, String> {

		private ProgressBar mProgressBar;

		@Override
		protected void onPreExecute() {
			mProgressBar = (ProgressBar) findViewById(R.id.ProgressBar);
			mProgressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(mUrl);
				URLConnection connection = url.openConnection();
				connection.connect();

				int fileLength = connection.getContentLength();

				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = openFileOutput("temp.gif", MODE_PRIVATE);

				byte data[] = new byte[1024];
				long total = 0;
				int count;
				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					publishProgress((int) (total * 100 / fileLength));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();
				
				InputStream in = openFileInput("temp.gif");
				OutputStream out = openFileOutput(mFilename, MODE_PRIVATE);

				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				out.flush();
				out.close();
				in.close();

				return null;
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				return null;
			}
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			mProgressBar.setProgress((int) progress[0]); 
		}

		@Override
		protected void onPostExecute(String filepath) {
			Log.d(TAG, "onPostExecute()");
			mProgressBar.setVisibility(View.GONE);
			mLargeImageView.loadImage("file://" + getFileStreamPath("temp.gif").getAbsolutePath());
		}
	}
}

