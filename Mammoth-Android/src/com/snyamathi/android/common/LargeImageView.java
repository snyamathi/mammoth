package com.snyamathi.android.common;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.AttributeSet;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LargeImageView extends WebView {

	public LargeImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setBackgroundColor(Color.BLACK);
		getSettings().setRenderPriority(RenderPriority.HIGH);
		getSettings().setBuiltInZoomControls(true);
		getSettings().setLoadWithOverviewMode(true);
		getSettings().setUseWideViewPort(true);

		if (android.os.Build.VERSION.SDK_INT > 10) {
			getSettings().setDisplayZoomControls(false);	
		}
	}

	public void loadImage(String uri) {
		String html = "<html><body><table width=\"100%\" height=\"100%\" align=\"center\" valign=\"center\"><tr><td><img src = \"" + uri + "\" width=\"100%\"/></td></tr></table</body></html>";
		loadDataWithBaseURL("", html, "text/html", "UTF-8", "");
	}
	
	public void loadImage(String uri, final Handler handler) {
		setWebViewClient(new WebViewClient() {
        	public void onPageFinished(WebView view, String url) {
        		handler.obtainMessage(1).sendToTarget();
        	}
        });
		loadImage(uri);
	}
}
