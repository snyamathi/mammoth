package com.snyamathi.android.common;

import java.io.File;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.snyamathi.Mammoth.Const;
import com.snyamathi.Mammoth.R;

public class LargeImageActivity extends Activity {

	private static final String TAG = LargeImageActivity.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_large_image);

		Intent intent = getIntent();
		String filename = intent.getStringExtra(Const.FILENAME);
		String asset = intent.getStringExtra(Const.ASSET);
		String url = intent.getStringExtra(Const.URL);
		int orientation = intent.getIntExtra(Const.ORIENTATION, -1);

		if (filename != null) {
			loadFile(filename);
		} else if (url != null) {
			loadUrl(url);
		} else if (asset != null) {
			loadAsset(asset);
		}
		
		if (orientation != -1) {
			setRequestedOrientation(orientation);
		}
	}

	private void loadFile(String filename) {
		String filepath = getFileStreamPath(filename).getAbsolutePath();
		if (!new File(filepath).canRead()) {
			Log.e(TAG, "Can't read / find file " + filepath);
			finish();
		}
		LargeImageView liv = (LargeImageView) findViewById(R.id.LargeImageView);
		liv.loadImage("file://" + filepath);
	}
	
	private void loadAsset(String asset) {
		LargeImageView liv = (LargeImageView) findViewById(R.id.LargeImageView);
		liv.loadImage("file:///android_asset/" + asset);
	}

	private void loadUrl(String url) {
		LargeImageView liv = (LargeImageView) findViewById(R.id.LargeImageView);
		final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading. Please wait...", true);
		dialog.setCancelable(true);
		dialog.show();
		liv.loadImage(url, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				dialog.dismiss();
			}
		});
	}
}
