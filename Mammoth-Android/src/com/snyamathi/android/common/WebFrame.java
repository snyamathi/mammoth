package com.snyamathi.android.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.ZoomDensity;

import com.snyamathi.Mammoth.Const;

public class WebFrame extends Activity {
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		Intent intent = getIntent();
		String url = intent.getStringExtra(Const.URL);
		
		WebView webView = new WebView(this);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setUseWideViewPort(false);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setDefaultZoom(ZoomDensity.FAR);
		webView.setWebViewClient(new WebViewClient() {
        	public void onPageFinished(WebView view, String url) {
    			setProgressBarIndeterminateVisibility(false);
        	}
        });
		
		setContentView(webView);
		setProgressBarIndeterminateVisibility(true);
		webView.loadUrl(url);		
	}
}