package com.snyamathi.android.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.os.Environment;
import android.util.Log;

public class DbUtils {
	
	private static final String TAG = DbUtils.class.getSimpleName();
	
	public static void pullDatabase() {
		Log.d(TAG, "pullDatabase()");
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			if (sd.canWrite()) {
				String currentDBPath = "//data//com.snyamathi.Mammoth//databases//MammothDb";
				String backupDBPath = "MammothDb";
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, backupDBPath);

				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB).getChannel();
					FileChannel dst = new FileOutputStream(backupDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}

}
